package main

import (
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/gsora/uptg-ng/upwork"
)

const updateMsgTemplate string = `
<b>%s</b>

<a href="%s">Link</a>
<b>Budget</b>: <i>%s</i>
<b>Posted on</b>: <i>%s</i>
`

func sendUpdates(updates []upwork.Update) {
	for _, update := range updates {
		msg := tgbotapi.NewMessage(telegramDest, assembleMessage(update))
		msg.ParseMode = tgbotapi.ModeHTML

		_, err := bot.Send(msg)
		if err != nil {
			log.Println(err)
		}
	}
}

func assembleMessage(update upwork.Update) string {
	return fmt.Sprintf(
		updateMsgTemplate,
		update.Title,
		update.URL,
		update.Budget,
		update.PostedOn,
	)
}
