FROM golang:latest as builder 

LABEL maintainer="Gianguido (gsora) Sorà"

WORKDIR /uptg-ng

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux go build -a -installsuffix cgo -o /go/bin/uptg-ng .

######## Start a new stage from scratch #######
FROM alpine:latest  

RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=builder go/bin/uptg-ng .

CMD ["./uptg-ng"]
