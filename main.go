package main

import (
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/gsora/uptg-ng/upwork"
)

var (
	telegramAPIKey string
	telegramDest   int64
	upworkURL      string
	redisEndpoint  string
	upworkSession  *upwork.Session
	bot            *tgbotapi.BotAPI
)

func main() {

	log.Println("uptg-ng started!")

	for {
		select {
		case upErr := <-upworkSession.Errors:
			// just log errors, don't send 'em over
			log.Println(upErr)
		case updates := <-upworkSession.Updates:
			sendUpdates(updates)
		}
	}
}
