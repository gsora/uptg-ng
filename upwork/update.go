package upwork

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
)

// Update signals a job fetched through Upwork RSS services.
type Update struct {
	Title    string
	URL      string
	Budget   string
	PostedOn string
}

// MarshalBinary implements encoding.BinaryMarshaler for Update.
func (u Update) MarshalBinary() ([]byte, error) {
	return json.Marshal(&u)
}

// Hash returns SHA-256 of an Update instance.
func (u Update) Hash() string {
	payload := u.Title + u.URL + u.Budget
	sum := sha256.Sum256([]byte(payload))
	return fmt.Sprintf("%x", sum)
}