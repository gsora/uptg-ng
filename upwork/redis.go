package upwork

import (
	"time"

	"golang.org/x/xerrors"
)

// redisEntryTTL represents the predefined amount of time an Update must
// be kept in Redis before getting deleted.
const redisEntryTTL time.Duration = (7 * 24) * time.Hour

// writeIfNotPresent writes a slice of Update's to Redis if it isn't already saved.
// It returns a slice containing all the Updates in u which
// weren't contained in Redis.
func (s Session) writeIfNotPresent(updates []Update) ([]Update, error) {
	var newUpdates []Update
	var err error

	for _, u := range updates {
		hash := u.Hash()
		found := s.redisConn.Exists(hash).Val()
		if found == 0 { // if it wasn't already contained in Redis, add it
			newUpdates = append(newUpdates, u)
			err = s.redisConn.Set(hash, u, redisEntryTTL).Err()
			if err != nil {
				return []Update{}, xerrors.Errorf("cannot add updates to redis: %w", err)
			}
		}
	}

	return newUpdates, nil
}
