package upwork

import (
	"regexp"
	"strings"
)

const (
	postedOnRegexp string = "<br /><b>Posted On</b>: (.+)UTC"
	budgetRegexp   string = `<br /><b>Budget</b>: \$(\d+)`
)

var (
	budget   *regexp.Regexp
	postedOn *regexp.Regexp
)

func setupRegexp() {
	budget = regexp.MustCompile(budgetRegexp)
	postedOn = regexp.MustCompile(postedOnRegexp)
}

func fetchJobInfo(input string) (b, po string) {
	if budget == nil || postedOn == nil {
		setupRegexp()
	}

	budgetMatch := budget.FindStringSubmatch(input)
	if len(budgetMatch) >= 2 {
		b = budgetMatch[1] + "$"
	} else if budgetMatch == nil || len(budgetMatch) <= 2 {
		b = "not provided"
	}

	postedOnMatch := postedOn.FindStringSubmatch(input)
	if len(postedOnMatch) >= 2 {
		po = postedOnMatch[1] + " UTC"
	} else if postedOnMatch == nil || len(postedOnMatch) <= 2 {
		po = "not provided"
	}

	return
}

func splitAndFirst(s string, field string) string {
	ss := strings.Split(s, field)
	if len(ss) > 1 {
		return ss[1]
	}

	return ""
}
