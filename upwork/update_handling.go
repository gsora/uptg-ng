package upwork

import (
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
	"golang.org/x/xerrors"
)

func (s Session) getUpdates() ([]Update, error) {
	p := gofeed.NewParser()

	feedData, err := p.ParseURL(s.feedURL.String())

	if err != nil {
		return []Update{}, xerrors.Errorf("could not retrieve update because of feed retrieval error: %w", err)
	}

	updates := []Update{}

	for _, job := range feedData.Items {
		u := Update{}
		u.Title = strings.TrimSuffix(job.Title, " - Upwork")

		u.URL = job.Link
		u.Budget, u.PostedOn = fetchJobInfo(job.Content)

		updates = append(updates, u)
	}

	return updates, nil
}

func (s *Session) lifecycle() {
	for {
		time.Sleep(10 * time.Second)
		updates, err := s.getUpdates()
		if err != nil {
			s.Errors <- xerrors.Errorf("could not retrieve updates: %w", err)
			continue
		}

		newUpdates, err := s.writeIfNotPresent(updates)
		if err != nil {
			s.Errors <- err
			continue
		}

		s.Updates <- newUpdates
	}
}
