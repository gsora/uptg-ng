package upwork

import (
	"net/url"

	"github.com/go-redis/redis"
	"golang.org/x/xerrors"
)

// Session is a connection to Upwork RSS services.
// It holds data and channels used by its lifecycle manager to fetch
// newly-available jobs and signal errors.
type Session struct {
	feedURL   *url.URL
	Updates   chan []Update
	Errors    chan error
	redisConn *redis.Client
}

// NewSession returns a new Session with the given Upwork RSS URL.
func NewSession(feedURL string, redisEndpoint string) (*Session, error) {
	var err error
	s := Session{}

	// check if feedURL is valid
	s.feedURL, err = url.Parse(feedURL)
	if err != nil {
		return nil, xerrors.Errorf("cannot parse feedURL: %w", err)
	}

	// initialize a Redis connection object
	s.redisConn = redis.NewClient(&redis.Options{
		Addr:     redisEndpoint,
		Password: "",
		DB:       0,
	})

	// test if Redis replies and works
	_, err = s.redisConn.Ping().Result()
	if err != nil {
		return nil, xerrors.Errorf("cannot connect to redis instance: %w", err)
	}

	s.Updates = make(chan []Update)
	s.Errors = make(chan error)

	// initialize state for the first time
	updates, err := s.getUpdates()
	if err != nil {
		return nil, xerrors.Errorf("could not initialize internal state: %w", err)
	}

	// create an initial update state
	// all updates that came after those will be notified
	_, err = s.writeIfNotPresent(updates)
	if err != nil {
		return nil, xerrors.Errorf("cannot build initial state due to redis error: %w", err)
	}

	// start the main bot lifecycle
	go s.lifecycle()

	return &s, nil
}
