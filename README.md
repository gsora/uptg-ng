# `uptg-ng`

Receive new Upwork jobs through Telegram.

## Requirements

 - a Telegram bot API token, which can be obtained via `@BotFather`
 - your Telegram user ID, obtainable by sending a private message to `@userinfobot`
 - your personal Upwork RSS/Atom URL 
 - a Redis instance

## Configuration

`uptg-ng` can be configured through some environment variables:

|Environment variable   	|Mandatory   	|Description   	|
|---	|---	|---	|
|`TG_APIKEY`   	|yes  	|Telegram bot API token   	|
|`UPWORK_URL`   	|yes   	|Upwork RSS/Atom URL   	|
|`TG_DEST`   	|yes   	|Redis endpoint address, defaults to `127.0.0.1:6379`   	|
|`REDIS_ENDPOINT`   	|no   	|Telegram user ID   	|

## Docker

`uptg-ng` is Docker-ready, plus a `docker-compose.yml` is provided to be able to setup the environment in a quick and easy way.

Drop your environment variable settings in a `.env` file and you're ready to go.