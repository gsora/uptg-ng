package main

import (
	"log"
	"os"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/gsora/uptg-ng/upwork"
)

func init() {
	var err error

	env()

	upworkSession, err = upwork.NewSession(upworkURL, redisEndpoint)
	if err != nil {
		log.Fatal(err)
	}

	bot, err = tgbotapi.NewBotAPI(telegramAPIKey)
	if err != nil {
		log.Fatal(err)
	}
}

func env() {
	var err error

	telegramAPIKey = os.Getenv("TG_APIKEY")
	if telegramAPIKey == "" {
		log.Fatal("missing Telegram API key in environment variable TG_APIKEY")
	}

	upworkURL = os.Getenv("UPWORK_URL")
	if upworkURL == "" {
		log.Fatal("missing Upwork RSS URL in environment variable UPWORK_URL")
	}

	telegramDestStr := os.Getenv("TG_DEST")
	if telegramDestStr == "" {
		log.Fatal("missing Telegram user destination ID in environment variable TG_DEST")
	}

	telegramDest, err = strconv.ParseInt(telegramDestStr, 10, 64)
	if err != nil {
		log.Fatal("cannot parse Telegram user destination ID:", err.Error())
	}

	redisEndpoint = os.Getenv("REDIS_ENDPOINT")
	if redisEndpoint == "" {
		redisEndpoint = "127.0.0.1:6379"
	}
}
